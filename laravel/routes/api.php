<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/**
 * AUTH
 */

// DO NOT CHECKING AUTHORIZATION
Route::post('auth/register', 'UserController@register');
Route::post('auth/login', 'UserController@authenticate');
//  CHECKING AUTHORIZATION
Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::post('user.changePassword', 'UserController@changePassword');
    Route::post('user.changeAddress', 'UserController@changeAddress');
    Route::post('stock.addToBucket', 'Stock@addToBucket');
    Route::post('stock.deleteFromBucket', 'Stock@deleteFromBucket');
    Route::post('stock.confirmBuying', 'Stock@confirmBuying');
    Route::post('stock.confirmDelivery', 'Stock@confirmDelivery');
    Route::post('stock.rateProduct', 'Stock@rateProduct');
});

/**
 * STOCK
 */

Route::get('stock.getCategories', 'Stock@getCategories');
Route::get('stock.getProducts', 'Stock@getProducts');
Route::get('stock.getProductById', 'Stock@getProductById');
Route::get('stock.search', 'Stock@search');
Route::get('stock.getTypes', 'Stock@getTypes');


/**
 * ADMIN
 */
//TODO: ADD EDIT!!!
Route::group(
    ['middleware' => ['jwt.verify']],
    function () {
        Route::get('admin.getUsers', 'Admin@getUsers');
// Route::get('admin.getUsers', 'Admin@getUsers'); TODO: By ID
        Route::post('admin.addUser', 'Admin@addUser');
        Route::post('admin.deleteUser', 'Admin@deleteUser');

        Route::get('admin.getCategories', 'Admin@getCategories');
        Route::post('admin.addCategory', 'Admin@addCategory');
        Route::post('admin.deleteCategory', 'Admin@deleteCategory');

        Route::get('admin.getTags', 'Admin@getTags');
        Route::post('admin.addTag', 'Admin@addTag');
        Route::post('admin.deleteTag', 'Admin@deleteTag');

        Route::get('admin.getSizes', 'Admin@getSizes');
        Route::post('admin.addSize', 'Admin@addSize');
        Route::post('admin.deleteSize', 'Admin@deleteSize');

        Route::post('admin.addProduct', 'Admin@addProduct');
        Route::post('admin.deleteProduct', 'Admin@deleteProduct');

        Route::post('admin.addProduct', 'Admin@addProduct');
        Route::post('admin.deleteProduct', 'Admin@deleteProduct');

        Route::get('admin.getDeliveries', 'Admin@getDeliveries');
        Route::get('admin.getDeliveryById', 'Admin@getDeliveryById');
        Route::post('admin.addDelivery', 'Admin@addDelviery');
        Route::post('admin.deleteDelivery', 'Admin@deleteDelivery');
    }
);