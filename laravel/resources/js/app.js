import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';

import {
    library
} from '@fortawesome/fontawesome-svg-core'
import {
    faStar,
    faShoppingCart,
    faUserAlt,
    faSortAmountDown,
    faSortDown
} from '@fortawesome/free-solid-svg-icons'
import {
    FontAwesomeIcon
} from '@fortawesome/vue-fontawesome'

// import Router from 'vue-router'
import Main from './components/main.vue'
import Catalogue from './components/Catalogue'
import Login from './components/Login'
import Registration from './components/Registration'
import Product from './components/Product'
import Admin from './components/admin'
import Cabinet from './components/Cabinet'
import Cart from './components/Cart'
import Offer from './components/Offer'
import Contacts from './components/Contacts'
import Shops from './components/Shops'
import Delivery from './components/Delivery'
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = 'http://127.0.0.1:8000/api';

library.add([faShoppingCart, faUserAlt, faStar, faSortAmountDown, faSortDown])

Vue.component('fai', FontAwesomeIcon)

const router = new VueRouter({
    mode: "history",
    routes: [{
            path: '/',
            name: 'Main',
            component: Main
        },
        {
            path: '/catalogue',
            name: 'Catalogue',
            component: Catalogue,
            props: true
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/registration',
            name: 'Registration',
            component: Registration
        },
        {
            path: '/product',
            name: 'Product',
            component: Product
        },
        {
            path: '/cabinet/',
            name: 'Cabinet',
            component: Cabinet
        },
        {
            path: '/cart',
            name: 'Cart',
            component: Cart
        },
        {
            path: '/cabinet/admin',
            name: 'Admin',
            component: Admin
        },
        {
            path: '/delivery',
            name: 'Delivery',
            component: Delivery
        },
        {
            path: '/contacts',
            name: 'Contacts',
            component: Contacts
        },
        {
            path: '/shops',
            name: 'Shops',
            component: Shops
        },
        {
            path: '/offer',
            name: 'Offer',
            component: Offer
        },
    ]
});
Vue.router = router
Vue.use(require('@websanova/vue-auth'), {
    rolesVar: 'role_id',
    loginData: {
        url: `http://127.0.0.1:8000/api/auth/login`,
        fetchUser: true
    },
    refreshData: {
        url: `http://127.0.0.1:8000/api/user`,
        enabled: false
    },
    fetchData: {
        url: `http://127.0.0.1:8000/api/user`,
        enabled: true
    },
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

App.router = Vue.router
new Vue({
    el: '#app',
    components: {
        App
    },
    router,
});
