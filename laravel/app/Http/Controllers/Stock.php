<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Product;
use App\Models\Bucket;
use App\Models\Delivery;
use App\Models\Rating;

use App\Models\Delivery_Type;
use App\Models\Payment_Type;
use App\Models\Status_Type;



use Illuminate\Support\Facades\Validator;


use JWTAuth;



class Stock extends Controller
{
    public function getCategories(Request $request)
    {
        $category = Category::get();
        return response()->json($category);
    }

    public function getProducts(Request $request)
    {
        if ($request->category > 0) {
            $products = Product::where('category_id', $request->category)
                ->where('type', 'like', ($request->type > 0 ? $request->type : '%%'))
                ->get()
                ->sortByDesc($request->sort_by ? $request->sort_by : 'rating')->values()->all();
        } else {
            $products = Product::get()
                ->sortByDesc($request->sort_by ? $request->sort_by : 'rating')->values()->all();
        }
        return response()->json($products);
    }

    public function getProductById(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        if ($request->id > 0) {
            $product = Product::find($request->id);
        }

        return response()->json($product);
    }

    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ne' => 'string',
            'p_min' => 'integer',
            'p_max' => 'integer',
            'r_max' => 'integer',
            'r_max' => 'integer',
            'rating' => 'integer',
            'isAviable' => 'boolean',
            'sort_by' => 'string',
            'offset' => 'integer',
            'count' => 'integer',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $products = Product::whereHas('tags', function ($tag) use ($request) {
            $tag->where('name', 'like', "%{$request->t}%");
        })
            ->where('product_name', 'like', "%{$request->n}%")
            ->where('price', '>=', ($request->p_min ? $request->p_min : 0))
            ->where('price', '<=', ($request->p_max ? $request->p_max : 999999))
            ->where('rating', '>=', ($request->r_min ? $request->r_min : 0))
            ->where('rating', '<=', ($request->r_max ? $request->r_max : 999999))
            ->where('isAviable', '=', $request->isAviable ? $request->isAviable : 1)
            ->whereHas('sizes', function ($size) use ($request) {
                $size->where('letter', 'like', $request->s ? "%{$request->s}%" : "%%")
                    ->orWhere('number', '=', $request->s);
            })
            ->orderBy($request->sort_by ? $request->sort_by : 'id', 'ASC')
            ->skip($request->offset ? $request->offset : 0)
            ->limit($request->count ? $request->count : 999)
            ->get();
        return response()->json($products);
    }

    public function addtoBucket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'integer|required',
            'size' => 'integer|required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $toBucket = new Bucket([
            'user_id' => JWTAuth::user()->id,
            'product_id' => $request->product_id,
            'size' => $request->size,
        ]);

        $toBucket->save();

        return response()->json(JWTAuth::user());
    }

    public function deleteFromBucket(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'integer|required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $bucket = Bucket::find($request->id);
        $bucket->delete();
        return response()->json(JWTAuth::user());
    }

    public function confirmBuying(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'integer|required',
            'delivery_type' => 'integer|required|min:1',
            'payment_type' => 'integer|required|min:1',
            sd
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        global $request;
        JWTAuth::user()->bucket->map(function ($bucket) {
            global $request;
            $delivery = new Delivery([
                'user_id' => JWTAuth::user()->id,
                'product_id' => $bucket->product_id,
                'size' => $bucket->size,
                'delivery_type' => $request->delivery_type,
                'payment_type' => $request->payment_type,
                'address' => JWTAuth::user()->def_address,
            ]);
            $delivery->save();
            Bucket::find($bucket->id)->delete();
        });
        return JWTAuth::user();
    }

    public function confirmDelivery(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'integer|required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $delivery = Delivery::find($request->id);
        $delivery->update([
            "status" => 5
        ]);
        return response()->json($delivery);
    }

    public function getTypes()
    {
        $types = [
            "delivery_types" => Delivery_Type::get(),
            "payment_types" => Payment_Type::get(),
            "status_types" => Status_Type::get()
        ];

        return response()->json($types);
    }

    public function rateProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'integer|required',
            'rating' => 'integer|required|min:1|max:5',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $rating = Rating::where('user_id', JWTAuth::user()->id)
            ->where('product_id', $request->product_id)
            ->first();
        if ($rating) {
            $rating->update([
                'rating' => $request->rating
            ]);
        } else {
            $rating = new Rating([
                'user_id' => JWTAuth::user()->id,
                'product_id' => $request->product_id,
                'rating' => $request->rating
            ]);
            $rating->save();
        }
        return $rating;
    }
}
