<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Size;
use App\Models\Product;
use App\Models\Delivery;

use App\Models\Category_Product;
use App\Models\product_tag;
use App\Models\Product_Size;

use JWTAuth;


class Admin extends Controller
{

    /**
     * USERS
     */
    public function getUsers()
    {
        return response()->json(User::get());
        if (JWTAuth::check() && JWTAuth::user()->role_id === 1) {
        } else {
            return 'Not admin';
        }
    }

    public function addUser(Request $request)
    {
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make($request->password),
            'role_id' => $request->role_id,
        ]);
        $user->save();
        return $user;
    }

    public function deleteUser(Request $request)
    {
        $user = User::find($request->id);
        $user->delete();
        return 'success';

    }
    //TODO: EDITUSER()
    /**
     * STOCK
     */

    public function getCategories(Request $request)
    {
        return response()->json(Category::get());
    }

    public function addCategory(Request $request)
    {
        $category = new Category([
            'category_name' => $request->category_name,
        ]);

        $category->save();
        return $category;
    }

    public function deleteCategory(Request $request)
    {
        $category = Category::find($request->id);
        $category->delete();
        return 'success';
    }
     //TODO: EDITCATEGORY()

    public function getTags()
    {
        return response()->json(Tag::get());
    }

    public function addTag(Request $request)
    {
        $tag = new Tag([
            'name' => $request->name
        ]);
        $tag->save();
        return $tag;
    }

    public function deleteTag(Request $request)
    {
        $tag = Tag::find($request->id);
        $tag->delete();
        return 'success';
    }
     //TODO: editTag()




    public function getSizes()
    {
        return response()->json(Size::get());
    }

    public function addSize(Request $request)
    {
        $size = new Size([
            'letter' => $request->letter,
            'number' => $request->number
        ]);
        $size->save();
        return $size;
    }

    public function deleteSize(Request $request)
    {
        $size = Size::find($request->id);
        $size->delete();
        return 'success';


    }




    public function addProduct(Request $request)
    {
        $product = new Product([
            'product_name' => $request->product_name,
            'price' => $request->price,
            'isAviable' => $request->isAviable
        ]);
        $product->save();

        // $category_product = new Category_Product([
        //     'product_id' => $product -> id,
        //     'category_id' => $request -> category_id
        // ]);
        // $category_product -> save();

        $product_tag = new Product_tag([
            'product_id' => $product->id,
            'tag_id' => $request->tag_id
        ]);
        $product_tag->save();

        $product_size = new Product_size([
            'product_id' => $product->id,
            'size_id' => $request->size_id
        ]);
        $product_size->save();

        return response()->json($product);
        //TODO: image upload

    }

    public function deleteProduct(Request $request)
    {
        $product = Product::find($request->id);
        $product->delete();

        $tag = product_tag::where('product_id', '=', $request->id);
        $tag->delete();

        $size = Product_size::where('product_id', '=', $request->id);
        $size->delete();

        return 'success';
    }

    //EDIT Product


    public function getDeliveries(Request $request)
    {
        return response()->json(Delivery::get());
    }

    public function getDeliveryById(Request $request)
    {
        $delivery = Delivery::find($request->id);
        return response()->json($delivery);
    }

    public function addDelivery(Request $request)
    {
        $delivery = new Delivery([
            'user_id' => $request->user_id,
            'product_id' => $request->product_id,
            'comment' => $request->comment,
            'status' => $request->status,
            'address' => $request->address,
        ]);
        $delivery->save();
        return $delivery;
    }

    public function deleteDelivery(Request $request)
    {
        $delivery = Delivery::find($request->id);
        $delivery->delete();
        return $delivery;
    }
}
