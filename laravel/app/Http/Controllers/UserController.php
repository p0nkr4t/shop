<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
        public function authenticate(Request $request)
        {
                $credentials = $request->only('email', 'password');
                if (!$token = JWTAuth::attempt($credentials)) {
                        return response([
                                'status' => 'error',
                                'error' => 'invalid.credentials',
                                'msg' => 'Invalid Credentials.'
                        ], 400);
                }
                return response([
                        'status' => 'success',
                        'token' => $token,
                        'user' => JWTAuth::user()
                ])->header('Authorization', $token);
        }

        public function register(Request $request)
        {
                $validator = Validator::make($request->all(), [
                        'name' => 'required|string|max:255',
                        'email' => 'required|string|email|max:255|unique:users',
                        'password' => 'required|string|min:3|confirmed',
                        'def_address' => 'required|string|min:3',
                        'phone_number' => 'required', //TODO: make phone validator
                ]);

                if ($validator->fails()) {
                        return response()->json($validator->errors()->toJson(), 400);
                }

                $user = User::create([
                        'name' => $request->get('name'),
                        'email' => $request->get('email'),
                        'password' => Hash::make($request->get('password')),
                        'def_address' => $request->get('def_address'),
                        'phone_number' => $request->get('phone_number'),
                ]);

                $token = JWTAuth::fromUser($user);

                return response()->json(compact('user', 'token'), 201);
        }

        public function getAuthenticatedUser()
        {
                try {

                        if (!$user = JWTAuth::parseToken()->authenticate()) {
                                return response()->json(['user_not_found'], 404);
                        }

                } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                        return response()->json(['token_expired'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                        return response()->json(['token_invalid'], $e->getStatusCode());

                } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                        return response()->json(['token_absent'], $e->getStatusCode());

                }

                return response()->json(['data' => JWTAuth::user()]);
        }

        public function changePassword(Request $request)
        {
                $validator = Validator::make($request->all(), [
                        'now_password' => 'required|string|min:3',
                        'new_password' => 'required|string|min:3|confirmed',
                ]);

                if ($validator->fails()) {
                        return response()->json($validator->errors()->toJson(), 400);
                }
                $credentials = [
                        'email' => JWTAuth::user()->email,
                        'password' => $request->now_password
                ];

                if (!$token = JWTAuth::attempt($credentials)) {
                        return response([
                                'status' => 'error',
                                'error' => 'invalid.credentials',
                                'msg' => 'Invalid Credentials.'
                        ], 400);
                }

                //TODO: Check now password
                $user = User::find(JWTAuth::user()->id);
                $user->update([
                        'password' => Hash::make($request->new_password),
                ]);
                return $user;
        }

        public function changeAddress(Request $request)
        {
                //TODO: VALIDATE DATA
                $user = User::find(JWTAuth::user()->id);
                $user->update([
                        'def_address' => $request->new_address,
                ]);
                return $user;
        }
}