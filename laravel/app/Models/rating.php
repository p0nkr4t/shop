<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class rating extends Model
{
    protected $visible = ['id', 'user_id', 'rating'];
    protected $fillable = ['user_id', 'product_id', 'rating'];
}
