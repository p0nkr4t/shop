<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $fillable = ['letter', 'number'];
    protected $visible = ['id', 'letter', 'number'];
}
