<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class category_product extends Model
{
    protected $fillable = ['product_id', 'category_id'];
    // protected $table = 'centers';
}