<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Size;

class Delivery extends Model
{
    protected $fillable = ['user_id', 'product_id', 'size', 'comment', 'status', 'address', 'trackcode', 'closes_in', 'delivery_type', 'payment_type'];
    protected $appends = ['product'];


    public function getProductAttribute()
    {
        $product = Product::find($this->product_id);
        return $product;
    }
}
