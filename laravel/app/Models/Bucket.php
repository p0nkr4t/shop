<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Products;

class Bucket extends Model
{
    protected $fillable = ['user_id', 'product_id', 'size'];
    protected $appends = ['product'];


    public function getProductAttribute()
    {
        $product = Product::find($this->product_id);
        return $product;
    }

}
