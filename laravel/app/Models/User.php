<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'def_address', 'phone_number', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = array('bucket', 'deliveries');

    public function getBucketAttribute()
    {
            //TODO: return products data but not a ID of products
        return $this->bucket()->get();
    }

    public function getDeliveriesAttribute()
    {
            //TODO: return products data but not a ID of products
        return $this->deliveries()->get();
    }

    public function bucket()
    {
        return $this->HasMany('App\Models\Bucket');
    }

    public function deliveries()
    {
        return $this->HasMany('App\Models\Delivery');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}