<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // protected $visible = ['tags'];
    protected $hidden = [
        'updated_at',
    ];

    protected $fillable = ['product_name', 'price', 'isAviable'];
    protected $appends = array('tags', 'images', 'size', 'comments', 'rating');


    public function getImagesAttribute()
    {
        return $this->images()->get();
    }

    public function getTagsAttribute()
    {
        return $this->tags()->get();
    }

    public function getSizeAttribute()
    {
        return $this->sizes()->get();
    }

    public function getCommentsAttribute()
    {
        return $this->ratings()->get();
    }

    public function getRatingAttribute()
    {
        return round($this->ratings()->avg('rating'));
    }

    // public function getCategoryAttribute() {
    //     return $this -> Category() -> get();
    // }


    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    // public function category() {
    //     return $this -> belongsToMany('App\Models\Category');
    // }

    public function images()
    {
        return $this->belongsToMany('App\Models\Image');
    }

    public function sizes()
    {
        return $this->belongsToMany('App\Models\Size');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\rating');
    }
    public function ratings()
    {
        return $this->hasMany('App\Models\rating');
    }
}
