<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class product_size extends Model
{
    protected $fillable = ['product_id', 'size_id'];
}
