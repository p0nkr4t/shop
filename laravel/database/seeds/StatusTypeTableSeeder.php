<?php

use Illuminate\Database\Seeder;

use App\Models\Status_Type;

class StatusTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $st = new Status_Type([
            'name' => 'Проверка заказа'
        ]);
        $st->save();

        $st = new Status_Type([
            'name' => 'Ожидание оплаты'
        ]);
        $st->save();

        $st = new Status_Type([
            'name' => 'Отправка'
        ]);
        $st->save();

        $st = new Status_Type([
            'name' => 'В пути'
        ]);
        $st->save();

        $st = new Status_Type([
            'name' => 'Получен'
        ]);
        $st->save();
    }
}
