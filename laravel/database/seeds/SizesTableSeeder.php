<?php

use Illuminate\Database\Seeder;
use App\Models\Size;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->delete();
        $size = new Size([
            'letter' => 'S',
            'number' => '48',
        ]);
        $size->save();
        $size = new Size([
            'letter' => 'M',
            'number' => '50',
        ]);
        $size->save();
        $size = new Size([
            'letter' => 'L',
            'number' => '52',
        ]);
        $size->save();
    }
}
