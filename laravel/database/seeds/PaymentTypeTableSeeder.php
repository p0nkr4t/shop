<?php

use Illuminate\Database\Seeder;

use App\Models\Payment_type;

class PaymentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dp = new Payment_type([
            'name' => 'При получении наличными/картой ',
        ]);
        $dp->save();

        $dp = new Payment_type([
            'name' => 'Онлайн',
        ]);
        $dp->save();
    }
}
