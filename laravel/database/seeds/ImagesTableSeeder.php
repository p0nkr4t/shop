<?php

use Illuminate\Database\Seeder;
use App\Models\Image;


class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('images')->delete();
        $image = new Image([
            'src' => 'images/Riot_Division_Urban_Bomber-(2).png',
        ]);
        $image->save();

        $image = new Image([
            'src' => 'images/Street_Partizan_Softshell_Jacket-(5).png',
        ]);
        $image->save();
    }
}
