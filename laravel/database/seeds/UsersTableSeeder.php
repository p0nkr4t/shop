<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $user = new User([
            'name' => 'Admin',
            'def_address' => str_random(30),
            'email' => "admin" . '@gmail.com',
            'phone_number' => rand(11, 11),
            'password' => bcrypt('secret'),
            'role_id' => 1,
        ]);
        $user->save();
    }
}
