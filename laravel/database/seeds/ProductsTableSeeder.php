<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Size;
use App\Models\Product;
use App\Models\Delivery;


class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        DB::table('product_size')->delete();

        $product = new Product([
            'id' => 1,
            'product_name' => 'Jacket',
            'category_id' => 1,
            'type' => 1,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);
        $product->save();
        $product->sizes()->attach([1, 2, 3]);
        $product->images()->attach([1, 2]);

        $product = new Product([
            'id' => 2,
            'product_name' => 'Jacket2',
            'category_id' => 1,
            'type' => 1,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);
        $product->save();
        $product->sizes()->attach([1, 2, 3]);
        $product->images()->attach([1, 2]);

        $product = new Product([
            'product_name' => str_random(7),
            'category_id' => 1,
            'type' => 1,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);
        $product->save();
        $product->sizes()->attach([1, 2, 3]);
        $product->images()->attach([1, 2]);

        $product = new Product([
            'product_name' => str_random(7),
            'category_id' => 1,
            'type' => 2,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);
        $product->save();
        $product->sizes()->attach([1, 2, 3]);
        $product->images()->attach([1, 2]);

        $product = new Product([
            'product_name' => str_random(7),
            'category_id' => 1,
            'type' => 2,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);
        $product->save();
        $product->sizes()->attach([1, 2, 3]);
        $product->images()->attach([1, 2]);


        $product = new Product([
            'product_name' => str_random(7),
            'category_id' => 2,
            'type' => 3,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);

        $product = new Product([
            'product_name' => str_random(7),
            'category_id' => 2,
            'type' => 3,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);

        $product = new Product([
            'product_name' => str_random(7),
            'category_id' => 2,
            'type' => 4,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);
        $product->save();
        $product->sizes()->attach([1, 2, 3]);
        $product->images()->attach([1, 2]);

        $product = new Product([
            'product_name' => str_random(7),
            'category_id' => 3,
            'type' => 0,
            'price' => rand(100, 10000),
            'description' => str_random(20),
        ]);


    }
}
