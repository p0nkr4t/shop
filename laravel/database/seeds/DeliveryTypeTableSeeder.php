<?php

use Illuminate\Database\Seeder;

use App\Models\Delivery_Type;

class DeliveryTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = new Delivery_Type([
            'name' => 'Самовывоз',
        ]);
        $dt->save();

        $dt = new Delivery_Type([
            'name' => 'Курьерская доставка',
        ]);
        $dt->save();

        $dt = new Delivery_Type([
            'name' => 'Почта России',
        ]);
        $dt->save();
    }
}
