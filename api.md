# API METHODS:

## <span style="color:red">Методы у которых нет статуса [done] не работают.

</span>

# AUTH:

TODO: Написать документацию к авторизации

# STOCK:

> Методы магазина (stock) отвечают за получение и управление продуктами магазина (products).

---

## stock.getCategories

status: done

params: не принимает параметры

---

---

## stock.getProducts

status: done

params: categrory={id категории} (При отсутствии вернет все продукты)

---

---

## stock.getProductById

status: done

params: id={id продукта} (Обязателен)

---

# USER:

> Методы пользователя (user) отвечают за получение и изменения информации пользователя.

### Работа с магазином

user.sendToBucket

user.buyBucketProducts

user.sendVote

### Работа с информацией пользователя

user.register

user.login

user.logout

user.getMyInfo

user.getMyBucket

user.getMyDeliveries

user.updateMyInfo

## Admin:

> Методы админа (admin) отвечают за получение и изменения информации сайта в целом.

### Работа с Юзерами

admin.getUsers

admin.getUserById

admin.getDeliveries

admin.getDeliveryById

admin.updateUser

### Работа с продуктами

admin.getProducts

admin.getProductById

admin.addProduct

### Работа с Категориями

admin.getCategories

admin.addCategory

admin.updateCategory
